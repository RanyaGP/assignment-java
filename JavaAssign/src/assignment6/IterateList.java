package assignment6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IterateList {

	public static void main(String[] args) {
		List<Integer>myList=new ArrayList<Integer>();
		myList.add(35);
		myList.add(60);
		myList.add(80);
		
		System.out.println("Number of elements:");
		System.out.println(myList.size());
		
		System.out.println("While loop elements are:");
        int  count=0;
        while(myList.size()>count)   
        {
            System.out.println(myList.get(count));
            count++;
        }
        
		System.out.println("For Loop elements are:");
		
		for(int i=0;i<myList.size();i++) {
			System.out.println(myList.get(i));
		}
		
		System.out.println("Iterator elements are:");
		Iterator<Integer> itr = myList.iterator();

		while(itr.hasNext()){    //it will print next value from given list
			System.out.println(itr.next());
			}
		
		System.out.println("Advance For Loop elements are:");
		for(Object obj : myList) {   //object will store values in list
			System.out.println(obj);
			}
	}

}
