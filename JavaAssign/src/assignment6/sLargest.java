package assignment6;

import java.util.Scanner;

public class sLargest {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);    
        System.out.println("Enter the size of the array");
        int num=sc.nextInt();  
        
        int arr[]=new int[num];   //Declare array element
        System.out.println("Enter the array");  
        for(int i=0;i<num;i++)     //Initialize array
        {
           arr[i]=sc.nextInt();
        }
              
       for(int i=0;i<num;i++)     
       {
           for(int j=i+1;j<num;j++)    //it will compare with the rest of the elements
           {
               if(arr[i]<arr[j])     //Check the array element and swap
               {
                   int temp=arr[i];
                   arr[i]=arr[j];
                   arr[j]=temp;
               }
           }
       }
       
       System.out.println("Second Largest element is "+arr[1]);

   }
}