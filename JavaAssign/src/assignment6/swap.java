package assignment6;

import java.util.Scanner;

public class swap {

	public static void main(String[] args) {
		int temp,x,y;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value of x:");
		x=sc.nextInt();
		System.out.println("Enter the value of y:");
		y=sc.nextInt();
		temp=x;
		x=y;
		y=temp;
		System.out.println("After swapping of x value is:\n"+x+"\nAfter swapping of y value is:\n"+y);
	}

}
